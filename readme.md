# Simple static site

Simple boilerplate for generating a static site.

## Installation
  - clone the repo
  - run `npm install`
  - run `npm run dev`
  - open browser and navigate to `http://localhost:1337`

## Customizations

### Change port
Open `server.js` in your favourite editor and change the value on `line 4`

